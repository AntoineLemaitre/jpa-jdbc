package JPA.model.MaireClasses.Impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.function.Function;

import JPA.model.Departement;
import JPA.model.MaireClasses.Int.DepartementBDDServices;
import JPA.model.MaireClasses.Int.DepartementImporter;

public class DepartementImpoterIMP implements DepartementImporter{

	@Override
	public void importDepartement(String path) {
		// TODO Auto-generated method stub
		DepartementBDDServices departementDBService = new DepartementBDDServicesIMP();

		Function<String, Departement> instancesDepartement = ligne -> {
			Departement departement = new Departement();
			String nomDepartement = ligne.split(";")[0];
			String codeDepartement = ligne.split(";")[1];
			departement.setNomDepartement(nomDepartement);
			;
			departement.setCodeDepartement(codeDepartement);

			return departement;
		};

		try {
			File file = new File(path);
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			String line = "";
			while ((line = br.readLine()) != null) {
				departementDBService.writeDepartement(instancesDepartement.apply(line));
			}
			br.close();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
		
	}

}
