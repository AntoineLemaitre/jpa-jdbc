package JPA.model.MaireClasses.Impl;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.concurrent.atomic.AtomicLong;


import JPA.model.Commune;
import JPA.model.MaireClasses.Int.CommuneBDDServices;

public class CommuneBDDServicesIMP implements CommuneBDDServices {

	AtomicLong identifiant = new AtomicLong();
	long id = identifiant.get();
	
	@Override
	public Commune getCommuneById(String id) {
		// TODO Auto-generated method stub
		Commune commune = new Commune();
		String url = "jdbc:mysql://localhost:3306/tpJPA2";
		String user = "A";
		String password = "P";

		try (Connection connection = DriverManager.getConnection(url, user, password)) {

			Statement statement = connection.createStatement();
			ResultSet statut = statement.executeQuery("SELECT * FROM commune WHERE id = " + id);

			while (statut.next()) {
				commune.setId(Long.valueOf(statut.getString(5)));
				commune.setCodeINSEE(statut.getString(1));
				commune.setNomCommune(statut.getString(2));
				commune.setCodePostal(statut.getString(3));
				commune.setLibelleAcheminement(statut.getString(4));
			}

			return commune;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public Commune getCommuneByName(String name) {
		// TODO Auto-generated method stub
		Commune commune = new Commune();
		String url = "jdbc:mysql://localhost:3306/tpJPA2";
		String user = "A";
		String password = "P";

		try (Connection connection = DriverManager.getConnection(url, user, password)) {

			Statement statement = connection.createStatement();
			ResultSet statut = statement.executeQuery("SELECT * FROM commune WHERE nomCommune = '" + name + "'");

			while (statut.next()) {
				commune.setId(Long.valueOf(statut.getString(5)));
				commune.setCodeINSEE(statut.getString(1));
				commune.setNomCommune(statut.getString(2));
				commune.setCodePostal(statut.getString(3));
				commune.setLibelleAcheminement(statut.getString(4));
			}

			return commune;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public int countCommuneByCodeP(String codePostal) {
		// TODO Auto-generated method stub
		String url = "jdbc:mysql://localhost:3306/tpJPA2";
		String user = "A";
		String password = "P";
		
		try (Connection connection = DriverManager.getConnection(url, user, password)) {

			Statement statement = connection.createStatement();
			ResultSet statut = statement.executeQuery(
					"SELECT COUNT(*) AS TOTAL FROM commune WHERE codePostal REGEXP '^" + codePostal + "'");
			statut.next();

			return statut.getInt("TOTAL");

		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		}
	}

	@Override
	public void writeCommune(Commune commune) {
		// TODO Auto-generated method stub
		String url = "jdbc:mysql://localhost:3306/tpJPA2";
		String user = "A";
		String password = "P";
		

		try (Connection connection = DriverManager.getConnection(url, user, password)) {
			long id = identifiant.incrementAndGet();
			commune.setId(id);
			String codeINSEE = commune.getCodeINSEE();
			String nomCommune = commune.getNomCommune();
			String codePostal = commune.getCodePostal();
			String libelle = commune.getLibelleAcheminement();
			Statement statement = connection.createStatement();
			statement.executeUpdate("INSERT INTO commune VALUES ('" + codeINSEE + "', '" + nomCommune + "', '"
					+ codePostal + "', '" + libelle + "', '" + id + "')");

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	

	@Override
	public Commune getCommuneById(long id) {
		// TODO Auto-generated method stub
		return null;
	}

}
