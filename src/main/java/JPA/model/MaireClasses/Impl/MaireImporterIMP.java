package JPA.model.MaireClasses.Impl;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import JPA.model.MaireClasses.Maire;
import JPA.model.MaireClasses.Maire.Civilite;
import JPA.model.MaireClasses.Int.MaireImporter;

public class MaireImporterIMP implements MaireImporter{
	
	
	
	
	MaireBDDServicesIMP maireBDDServiceIMP = new MaireBDDServicesIMP();

	public MaireImporterIMP(MaireBDDServicesIMP maireBDDServiceIMP) {

		this.maireBDDServiceIMP = maireBDDServiceIMP;
	}

	public List<String> extractedElements(String fileName) {
		List<String> maireCSV = null;
		Path path = Paths.get(fileName);
		try (Stream<String> lines = Files.lines(path)) {

			maireCSV = lines.skip(1).collect(Collectors.toList());
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}

		return maireCSV;
	}

	public Civilite toCivility(String strings) {
		Civilite civilite = Civilite.valueOf(strings);
		return civilite;
	}

	public MaireImporterIMP() {

	}

	Function<String, Maire> convertToMaire = ligne -> {
		Maire maire = new Maire();
		String getCivility = ligne.split(";")[6].substring(1, 2);
		String getDOB = ligne.split(";")[7];

		try {
			Date convertedDate = new SimpleDateFormat("yyyy-mm-dd", Locale.ENGLISH).parse(getDOB);
			maire.setCivilite(toCivility(getCivility));
			maire.setDateDeNaissance(convertedDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return maire;
	};

	@Override
	public void importMaire(String path) {
		// TODO Auto-generated method stub
		List<String> lines = extractedElements(path);
		List<Maire> CollectToListOfMaire = lines.stream().map(convertToMaire).collect(Collectors.toList());
		maireBDDServiceIMP.writeMaire(CollectToListOfMaire);
	}

}
