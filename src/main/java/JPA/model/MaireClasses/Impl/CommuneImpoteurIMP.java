package JPA.model.MaireClasses.Impl;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.function.Function;

import JPA.model.Commune;
import JPA.model.MaireClasses.Int.CommuneBDDServices;
import JPA.model.MaireClasses.Int.CommuneImporter;

public class CommuneImpoteurIMP implements CommuneImporter{

	@Override
	public void importCommunes(String path) {
		// TODO Auto-generated method stub
		CommuneBDDServices communeDBService = new CommuneBDDServicesIMP();

		Function<String, Commune> instancesCommune = ligne -> {
			Commune commune = new Commune();
			String codeINSEE = ligne.split(";")[0];
			String nomCommune = ligne.split(";")[1];
			String codePostal = ligne.split(";")[2];
			String libelle = ligne.split(";")[4];
			commune.setCodeINSEE(codeINSEE);
			commune.setNomCommune(nomCommune);
			commune.setCodePostal(codePostal);
			commune.setLibelleAcheminement(libelle);

			return commune;
		};

		try {
			File file = new File(path);
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			String line = "";
			while ((line = br.readLine()) != null) {
				communeDBService.writeCommune(instancesCommune.apply(line));
			}
			br.close();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}

	}
		
	}


