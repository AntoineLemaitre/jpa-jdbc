package JPA.model.MaireClasses.Impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.concurrent.atomic.AtomicLong;
import JPA.model.Departement;
import JPA.model.MaireClasses.Int.DepartementBDDServices;

public class DepartementBDDServicesIMP implements DepartementBDDServices {
	AtomicLong identifiant = new AtomicLong();
	long id = identifiant.get();
	@Override
	public void writeDepartement(Departement departement) {
		// TODO Auto-generated method stub
		String url = "jdbc:mysql://localhost:3306/tpJPA2";
		String user = "A";
		String password = "P";

		try (Connection connection = DriverManager.getConnection(url, user, password)) {
			long id = identifiant.incrementAndGet();
			departement.setId(id);
			String nomDepartement = departement.getNomDepartement();
			String codeDepartement = departement.getCodeDepartement();
			Statement statement = connection.createStatement();
			statement.executeUpdate("INSERT INTO departement VALUES ('" + nomDepartement + "', '" + codeDepartement
					+ "', '" + id + "')");

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public Departement getDepartementByCP(String cp) {
		// TODO Auto-generated method stub
		Departement departement = new Departement();
		String url = "jdbc:mysql://localhost:3306/tpJPA2";
		String user = "A";
		String password = "P";
		try (Connection connection = DriverManager.getConnection(url, user, password)) {

			Statement statement = connection.createStatement();
			ResultSet statut = statement.executeQuery("SELECT * FROM departement WHERE codeDepartement = " + cp);

			while (statut.next()) {
				departement.setId(Long.valueOf(statut.getString(3)));
				departement.setNomDepartement(statut.getString(1));
				departement.setCodeDepartement(statut.getString(2));
			}

			return departement;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public Departement getDepartementByName(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int countCommuneByCP(String codePostal) {
		// TODO Auto-generated method stub
		return 0;
	}

}
