package JPA.model.MaireClasses.Impl;

import java.util.List;


import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import JPA.model.MaireClasses.Maire;
import JPA.model.MaireClasses.Int.MaireBDDServices;

public class MaireBDDServicesIMP implements MaireBDDServices {

	public MaireBDDServicesIMP() {

	}
	@Override
	public void writeMaire(List<Maire> ListMaires) {
		// TODO Auto-generated method stub
		if (ListMaires != null) {

			try {
				String persistenceUnitName = "tpJPA2";
				EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory(persistenceUnitName);
				EntityManager entityManager = entityManagerFactory.createEntityManager();

				entityManager.getTransaction().begin();
				for (Maire maire : ListMaires) {
					entityManager.persist(maire);
				}

				entityManager.getTransaction().commit();
			} catch (Exception e) {
				e.printStackTrace();
			}
	}


	}
}
