package JPA.model.MaireClasses;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import JPA.model.Commune;

@Entity
@Access(AccessType.FIELD)
public class Maire implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@OneToOne(mappedBy = "maire")
	private Commune commune;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	public enum Civilite {M, F}
	@Column(name = "civilite", length = 1)
	@Enumerated(EnumType.STRING)
	Civilite civilite;
	@Column(name = "dateDeNaissance", length = 10)
	@Temporal(TemporalType.DATE)
	Date dateDeNaissance = new Date();
	
	
	
	
	public Maire() {

	}
	public Maire(Civilite civilite, Date dateDeNaissance) {
		this.civilite = civilite;
		this.dateDeNaissance = dateDeNaissance;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public Date getDateDeNaissance() {
		return dateDeNaissance;
	}
	public void setDateDeNaissance(Date dateDeNaissance) {
		this.dateDeNaissance = dateDeNaissance;
	}
	public Civilite getCivilite() {
		return civilite;
	}
	public void setCivilite(Civilite civilite) {
		this.civilite = civilite;
	}
	@Override
	public String toString() {
		return "Maire [id=" + id + ", civilite=" + civilite + ", dateDeNaissance=" + dateDeNaissance + "]";
	}
	
	

}
