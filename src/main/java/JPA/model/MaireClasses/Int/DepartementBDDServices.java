package JPA.model.MaireClasses.Int;






import JPA.model.Departement;

public interface DepartementBDDServices {

	
	
	
	
	
	
	
	void writeDepartement(Departement departement);

	Departement getDepartementByCP(String cp);

	Departement getDepartementByName(String name);

	int countCommuneByCP(String codePostal);
}
