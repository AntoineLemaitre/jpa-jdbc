package JPA.model.MaireClasses.Int;

import JPA.model.Commune;

public interface CommuneBDDServices {
	Commune getCommuneById(long id);

	Commune getCommuneByName(String name);

	int countCommuneByCodeP(String codePostal);

	void writeCommune(Commune commune);

	Commune getCommuneById(String id);

}
