package JPA.model;

import java.io.Serializable;


import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import JPA.model.MaireClasses.Maire;

@Entity
@Access(AccessType.FIELD)
public class Commune implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	@OneToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH })
	private Maire maire;
	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REFRESH })
	private Departement departement;
	
	
	
	
	@Column(name = "Code_INSEE", length = 40)
	private String codeINSEE;
	@Column(name = "Nom_Commune", length = 40)
	private String nomCommune;
	@Column(name = "Code_Postal", length = 5)
	private String codePostal;
	@Column(name = "Libelle", length = 40)
	private String libelleAcheminement;
	
	public Maire getMaire() {
		return maire;
	}
	public void setMaire(Maire maire) {
		this.maire = maire;
	}
	public Departement getDepartement() {
		return departement;
	}
	public void setDepartement(Departement departement) {
		this.departement = departement;
	}
	public Commune() {

	}
	public Commune(String codeINSEE, String nomCommune, String codePostal, String libelleAcheminement) {
		this.codeINSEE = codeINSEE;
		this.nomCommune = nomCommune;
		this.codePostal = codePostal;
		this.libelleAcheminement = libelleAcheminement;
	}
	public long getId() {
		return id;
	}
	public void setId(long l) {
		this.id = l;
	}
	public String getCodeINSEE() {
		return codeINSEE;
	}
	public void setCodeINSEE(String codeINSEE) {
		this.codeINSEE = codeINSEE;
	}
	public String getNomCommune() {
		return nomCommune;
	}
	public void setNomCommune(String nomCommune) {
		this.nomCommune = nomCommune;
	}
	public String getCodePostal() {
		return codePostal;
	}
	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}
	public String getLibelleAcheminement() {
		return libelleAcheminement;
	}
	public void setLibelleAcheminement(String libelleAcheminement) {
		this.libelleAcheminement = libelleAcheminement;
	}
	
	

}
