package JPA.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import java.io.Serializable;

@Entity(name = "Departement")
@Access(AccessType.FIELD)
@NamedQueries({

		@NamedQuery(name = "getNumberOfTowns()", query = "SELECT COUNT(*) from Departement dep where dep.nomDepartement like 'Val-d'Oise'"),

		@NamedQuery(name = "getOldestMayor()", query = "SELECT MIN(dateDeNaissance) from Maire"),

		@NamedQuery(name = "getWomanRatio()", query = "SELECT COUNT(*) from Maire womenRatio where womenRatio.civilite like 'F'"), 
		})
public class Departement implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	@OneToMany(mappedBy = "departement", cascade = { CascadeType.PERSIST, CascadeType.REFRESH })
	private List<Commune> commune = new ArrayList<>();
	@Column(name = "nomDepartement", length = 50)
	private String nomDepartement;
	@Column(name = "codeDepartement", length = 5)
	private String codeDepartement;
	
	
	public Departement() {

	}
	public Departement(String nomDepartement, String codeDepartement) {
		this.nomDepartement = nomDepartement;
		this.codeDepartement = codeDepartement;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNomDepartement() {
		return nomDepartement;
	}
	public void setNomDepartement(String nomDepartement) {
		this.nomDepartement = nomDepartement;
	}
	public String getCodeDepartement() {
		return codeDepartement;
	}
	public void setCodeDepartement(String codeDepartement) {
		this.codeDepartement = codeDepartement;
	}
	public List<Commune> getCommune() {
		return commune;
	}
	public void setCommune(List<Commune> commune) {
		this.commune = commune;
	}
	@Override
	public String toString() {
		return " le Departement est [commune=" + commune + ", id=" + id + ", nomDepartement=" + nomDepartement
				+ ", codeDepartement=" + codeDepartement + "]";
	}

}
