package JPA.ex4;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import JPA.model.MaireClasses.Impl.MaireImporterIMP;
public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String persistenceUnitName = "tpJPA2";
		EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory(persistenceUnitName);
		EntityManager entityManager = entityManagerFactory.createEntityManager();

		String path = "files/maires-17-06-2014.csv";
		MaireImporterIMP maireImporterImpl = new MaireImporterIMP();
		maireImporterImpl.importMaire(path);

		Query numberOfTowns = entityManager.createNamedQuery("getNumberOfTowns()");
		Query oldestMayor = entityManager.createNamedQuery("getOldestMayor()");
		Query womanRation = entityManager.createNamedQuery("getWomanRatio()");

		System.out.println("NB villes : " + numberOfTowns.getMaxResults());
		System.out.println("Le + vieux de tous les departements : " + oldestMayor.getResultList());
		System.out.println("NB de femmes: " + womanRation.getMaxResults());

	}

}
