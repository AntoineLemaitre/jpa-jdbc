package Ex2;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//String url = "jdbc:sqlserver://localhost:3306/jdbc";
		String url = "jdbc:mysql://localhost:3306/jdbc";
		String user = "Antoine";
		String password = "";
		try (Connection connection = 
				DriverManager.getConnection(url, user, password);) {
			
			PreparedStatement statement = 
					connection.prepareStatement("select * from User");
			
			ResultSet resultSet = statement.executeQuery();
			
			while (resultSet.next()) {
				
				int id = resultSet.getInt("id");
				String name = resultSet.getString("name");
				int age = resultSet.getInt("age");
				
				System.out.printf("User: %d %s %d\n", id, name, age);
				
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
