package Ex1;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub


		String fileName = "files/laposte_hexasmal.csv";
		
		List<String> communesCSV = null;
		Path path = Path.of(fileName);
		try (Stream<String> lines = Files.lines(path);) {
			
			communesCSV = lines.skip(1).collect(Collectors.toList());
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		System.out.println("# communes = " + communesCSV.size());
		
		// List<Commune> communes = 
		Map<String, Long> collect = communesCSV.stream()
			.map(line -> line.split(";")[0])
			.collect(
					Collectors.groupingBy(
							Function.identity(), Collectors.counting()
					));
		
		collect.entrySet()
				.removeIf(entry -> entry.getValue() == 1);
//		collect.forEach(
//				(codePostal, value) -> System.out.printf("%s %d\n", codePostal, value)
//				);
		
		long count = communesCSV.stream()
			.map(line -> line.split(";")[2])
			.map(codePostal -> Integer.parseInt(codePostal))
			.count();
		System.out.println("count = " + count);
	}

}
